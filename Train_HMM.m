% Alphabet for randomized dining philosopher
Alphabet={'think','hungry','try','pick','drop','eat'};

% Read the training samples
dir = "randomized-dining-philosophers/LR/Traces/Training/";
filename="LR_3Phils.csv";

Data = readCSVData(dir+filename);

Tolerance = 1e-06;
Iterations = 1000;

% Model folders
modelFolder =  "randomized-dining-philosophers/LR/Trained-Models/3-Phils/";

% The maximum number of states to the trained HMM can have
n = 20;
% The log-likelihood of the trained HMM
logLH = zeros(1,n);
BIC = zeros(1,n);
AIC = zeros(1,n);

% Training models with k states, starting from 1 to n
for k=1:n
    % Random initialization of trans and emis from a uniform dist.
    trans=ones(k,k);
    emis=ones(k,length(Alphabet));
    for i=1:k
        rv = randfixedsum(k,1,1,0,1);
        trans(i,:)=rv';
        rv = randfixedsum(length(Alphabet),1,1,0,1);
        emis(i,:)=rv';
    end
    
    % applying the initial probability,
    rv = randfixedsum(k,1,1,0,1);
    p = rv';
    trans_hat=[0 p; zeros(size(trans,1),1) trans];
    emis_hat=[zeros(1,size(emis,2)); emis];
    
    % Running the training algorithm
    % Size of Training set
    SizeTraining=length(Data);
    [estTrans,estEmis]=hmmtrain(Data(1:SizeTraining),trans_hat,emis_hat,'Symbols',Alphabet, 'Tolerance', Tolerance, 'Maxiteration', iterations);
    disp('-----------------');
    output=sprintf('Number of states:%d\n Estimated HMM:',k);
    display(output);
    display(estTrans);
    display(estEmis);
    
    % Calculating the log-likelihood
    for i=1:SizeTraining
        [PSTATEs,logpseq]=hmmdecode(Data{i},estTrans,estEmis, 'Symbols', Alphabet);
        logLH(1,k) = logLH(1,k)+logpseq;
    end
    BIC(1,k) = log(SizeTraining)*(k*k+k*length(Alphabet))-2*logLH(1,k);
    AIC(1,k) = 2*(k*k+k*length(Alphabet))-2*logLH(1,k);
    
    % Storing the trans and emis matrices of the HMM that has the
    % maximum likelihood
    modelFileName= modelFolder + "HMM" + k + "Tolerance-" + num2str(Tolerance) + "-" + num2str(Iterations) + "-" + filename + ".mat";
    save(modelFileName,'estTrans', 'estEmis');
end
save(modelFolder+"logLH" + "-" + "Tolerance-" + num2str(Tolerance) + "-" + num2str(Iterations) + filename + ".mat",'logLH');
save(modelFolder+"BIC" + "Tolerance-" + num2str(Tolerance) + "-" + num2str(Iterations) + filename + ".mat",'BIC');
save(modelFolder+"AIC" + "-" + "Tolerance-" + num2str(Tolerance) + "-" + num2str(Iterations) + filename + ".mat",'AIC');
