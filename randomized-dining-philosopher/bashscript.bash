#!/bin/bash
for i in {1..100}
do
    timeout -s SIGINT 5s ./randomized-dining-philosophers 3
    timeout -s SIGINT 8s ./randomized-dining-philosophers 4
    timeout -s SIGINT 11s ./randomized-dining-philosophers 5
    timeout -s SIGINT 14s ./randomized-dining-philosophers 6
    timeout -s SIGINT 17s ./randomized-dining-philosophers 7
    timeout -s SIGINT 20s ./randomized-dining-philosophers 8
    timeout -s SIGINT 23s ./randomized-dining-philosophers 9
    timeout -s SIGINT 26s ./randomized-dining-philosophers 10	
done
