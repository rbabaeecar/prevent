/*
 * Randomized Dining Philosophers
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include  <signal.h>

/*
 * Constants
 */
#define FORK_TAKEN        0
#define FORK_FREE         1

#define COIN_HEADS        0
#define COIN_TAILS        1

#define SYM_NONE         -1
#define SYM_THINK         0
#define SYM_HUNGRY        1
#define SYM_TRY           2
#define SYM_PICK_FORK     3
#define SYM_DROP_FORKS    4
#define SYM_EAT           5

#define OUR_PHILOSOPHER   1

// The Heads bias for the biased coin that takes an integer value between 0-100
// The value of BIAS is 1-c in the paper
#define BIAS 0

// Our philosopher bias
#define BIAS_P 100

/* Symbols */
static char *sym_text[] = { "think", "hungry", "try", "pick", "drop", "eat" };

/*
 * Function prototypes
 */
static int flip_coin(void);
static int flip_Bcoin(int);
static void *philosopher(int);
static void *our_philosopher(int);
static void emit_symbol(const int);
static void error_out(const char *);
void     INThandler(int);

/*
 * Global variables
 */
int can_go_on;
int n_phil, n_fork;
pthread_mutex_t *fork_mutex;
int *phil_state, *fork_state;
FILE* output;

int main(int argc, char *argv[]) {
  //  The number of philosophers is passed to the program as the first parameter
  int N_PHILOSOPHERS = atoi(argv[1]);
  char filename[100];
  sprintf(filename,"trace_%dPhils_B%d.csv",N_PHILOSOPHERS, BIAS);
  output=fopen(filename,"a");
  fprintf(output,"\n");
  signal(SIGINT, INThandler);

  long i;
  pthread_t *thread;

  /* Number of philosophers equals number of forks */
  n_phil = n_fork = N_PHILOSOPHERS;

  /* Allocate space for the thread structures */
  if (!(thread = (pthread_t *) calloc(n_phil, sizeof(pthread_t))))
    error_out("calloc");

  /*
   * Allocate space for keeping philosopher and fork states.
   */
  if (!(phil_state = (int *) calloc(n_phil, sizeof(int))))
    error_out("calloc");

  if (!(fork_state = (int *) calloc(n_phil, sizeof(int))))
    error_out("calloc");

  if (!(fork_mutex = (pthread_mutex_t *) calloc(n_phil, sizeof(pthread_mutex_t))))
    error_out("calloc");

  /* Initialize fork states */
  for (i = 0; i < n_fork; i++)
    fork_state[i] = FORK_FREE;

  /* Set seed for the pseudo-random generator */
  srandom(time(NULL) * getpid());

  /* Initialize mutexes */
  for (i = 0; i < n_phil; i++)
    pthread_mutex_init(&fork_mutex[i], NULL);

  /* Create threads */
  for (i = 0; i < n_phil; i++)
    if (i == OUR_PHILOSOPHER)
      pthread_create(&thread[i], NULL, (void *(*)(void *))our_philosopher, (void *)i);
    else
      pthread_create(&thread[i], NULL, (void *(*)(void *))philosopher, (void *)i);

  /* Release the threads waiting on the primitive barrier */
  can_go_on = 1;

  /* Wait for the threads to finish */
  for (i = 0; i < n_phil; i++)
    pthread_join(thread[i], NULL);

  /* Free resources */
  for (i = 0; i < n_phil; i++)
    pthread_mutex_destroy(&fork_mutex[i]);

  free(fork_mutex);
  free(fork_state);
  free(phil_state);
  free(thread);

  return 0;
}

/*
 * We really want the threads to do some work,
 * not just keep fighting for the neighbor's fork.
 */
static void do_something(void)
{
    int i, n, *p;

    n = random() % 64 + 0x8000;
    for (i = 0; i < n; i++) {
        if ((p = (int *) malloc(1024))) {
            memset(p, random() % 0xff, 1024);
            free(p);
        }
    }
}

/*
 * Main loop executed by each philosopher
 */
static void *philosopher(int this_phil)
{
    int bias = BIAS;
    /* A primitive barrier */
    while (!can_go_on)
        continue;

    while (1) {

      do_something();
      switch (phil_state[this_phil]) {
        case 0: /* thinking */
          /* cannot stay thinking */
          phil_state[this_phil] = 1;
          break;

        case 1: /* hungry */
          /* draw randomly */
          if (flip_coin() == COIN_HEADS)
            phil_state[this_phil] = 2;
          else
            phil_state[this_phil] = 3;
          break;

        case 2: /* try to pick up left */
          pthread_mutex_lock(&fork_mutex[this_phil]);
          if (fork_state[this_phil] == FORK_FREE) {
            phil_state[this_phil] = 4;
            fork_state[this_phil] = FORK_TAKEN;
          }
          pthread_mutex_unlock(&fork_mutex[this_phil]);
          break;

        case 3: /* try to pick up right */
          pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
          if (fork_state[(this_phil + 1) % n_phil] == FORK_FREE) {
            phil_state[this_phil] = 5;
            fork_state[(this_phil + 1) % n_phil] = FORK_TAKEN;
          }
          pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
          break;

        case 4: /* got right, try to pick up left */
          pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
          if (fork_state[(this_phil + 1) % n_phil] == FORK_FREE) {
            phil_state[this_phil] = 8;
            fork_state[(this_phil + 1) % n_phil] = FORK_TAKEN;
          } else {
            if (flip_Bcoin(bias) == COIN_HEADS)
              phil_state[this_phil] = 6;
            else
              phil_state[this_phil] = 4;
          }
          pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
          break;

        case 5: /* got left, try to pick up right */
          pthread_mutex_lock(&fork_mutex[this_phil]);
          if (fork_state[this_phil] == FORK_FREE) {
            phil_state[this_phil] = 8;
            fork_state[this_phil] = FORK_TAKEN;
          } else {
            if (flip_Bcoin(bias) == COIN_HEADS)
              phil_state[this_phil] = 7;
            else
              phil_state[this_phil] = 5;
          }
          pthread_mutex_unlock(&fork_mutex[this_phil]);
          break;

        case 6: /* put down left */
          pthread_mutex_lock(&fork_mutex[this_phil]);
          phil_state[this_phil] = 1;
          fork_state[this_phil] = FORK_FREE;
          pthread_mutex_unlock(&fork_mutex[this_phil]);
          break;

        case 7: /* put down right */
          pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
          phil_state[this_phil] = 1;
          fork_state[(this_phil + 1) % n_phil] = FORK_FREE;
          pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
          break;

        case 8: /* got both forks */
          phil_state[this_phil] = 9;
          break;

        case 9: /* eat */
          phil_state[this_phil] = 10;
          break;

        case 10: /* drop the left fork */
          pthread_mutex_lock(&fork_mutex[this_phil]);
          phil_state[this_phil] = 11;
          fork_state[this_phil] = FORK_FREE;
          pthread_mutex_unlock(&fork_mutex[this_phil]);
          break;

        case 11: /* drop the right fork */
          pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
          phil_state[this_phil] = 0;
          fork_state[(this_phil + 1) % n_phil] = FORK_FREE;
          pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
          break;
        default:
          error_out("phil: incorrect philosopher state");
      }
    }
    return NULL;
}

/*
 * This is the instrumented variant of the 
 * philosopher() function, run by 'our' philosopher.
 */
static void *our_philosopher(int this_phil) {
  int bias = BIAS_P;
  /* A primitive barrier */
  while (!can_go_on)
    continue;

  while (1) {
    do_something();

    switch (phil_state[this_phil]) {
      case 0: /* thinking */
        emit_symbol(SYM_THINK);
        /* cannot stay thinking */
        phil_state[this_phil] = 1;
        break;

      case 1: /* hungry */
        /* draw randomly */
        emit_symbol(SYM_HUNGRY);
        if (flip_coin() == COIN_HEADS)
          phil_state[this_phil] = 2;
        else
          phil_state[this_phil] = 3;
        break;

      case 2: /* try to pick up left */
        emit_symbol(SYM_TRY);
        pthread_mutex_lock(&fork_mutex[this_phil]);
        if (fork_state[this_phil] == FORK_FREE) {
          phil_state[this_phil] = 4;
          fork_state[this_phil] = FORK_TAKEN;
        }
        pthread_mutex_unlock(&fork_mutex[this_phil]);
        break;

      case 3: /* try to pick up right */
        emit_symbol(SYM_TRY);
        pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
        if (fork_state[(this_phil + 1) % n_phil] == FORK_FREE) {
          phil_state[this_phil] = 5;
          fork_state[(this_phil + 1) % n_phil] = FORK_TAKEN;
        }
        pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
        break;

      case 4: /* got right, try to pick up left */
        emit_symbol(SYM_PICK_FORK);
        pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
        if (fork_state[(this_phil + 1) % n_phil] == FORK_FREE) {
          phil_state[this_phil] = 8;
          fork_state[(this_phil + 1) % n_phil] = FORK_TAKEN;
        } else {
          if (flip_Bcoin(bias) == COIN_HEADS)
            phil_state[this_phil] = 6;
          else
            phil_state[this_phil] = 4;
        }
        pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
        break;

      case 5: /* got left, try to pick up right */
        emit_symbol(SYM_PICK_FORK);
        pthread_mutex_lock(&fork_mutex[this_phil]);
        if (fork_state[this_phil] == FORK_FREE) {
          phil_state[this_phil] = 8;
          fork_state[this_phil] = FORK_TAKEN;
        } else {
          if (flip_Bcoin(bias) == COIN_HEADS)
            phil_state[this_phil] = 7;
          else
            phil_state[this_phil] = 5;
        }
        pthread_mutex_unlock(&fork_mutex[this_phil]);
        break;

      case 6: /* put down left */
        emit_symbol(SYM_DROP_FORKS);
        pthread_mutex_lock(&fork_mutex[this_phil]);
        phil_state[this_phil] = 1;
        fork_state[this_phil] = FORK_FREE;
        pthread_mutex_unlock(&fork_mutex[this_phil]);
        break;

      case 7: /* put down right */
        emit_symbol(SYM_DROP_FORKS);
        pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
        phil_state[this_phil] = 1;
        fork_state[(this_phil + 1) % n_phil] = FORK_FREE;
        pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
        break;

      case 8: /* got both forks */
        phil_state[this_phil] = 9;
        break;

      case 9: /* eat */
        emit_symbol(SYM_EAT);
        phil_state[this_phil] = 10;
        break;

      case 10: /* drop the left fork */
        emit_symbol(SYM_DROP_FORKS);
        pthread_mutex_lock(&fork_mutex[this_phil]);
        phil_state[this_phil] = 11;
        fork_state[this_phil] = FORK_FREE;
        pthread_mutex_unlock(&fork_mutex[this_phil]);
        break;

      case 11: /* drop the right fork */
        emit_symbol(SYM_DROP_FORKS);
        pthread_mutex_lock(&fork_mutex[(this_phil + 1) % n_phil]);
        phil_state[this_phil] = 0;
        fork_state[(this_phil + 1) % n_phil] = FORK_FREE;
        pthread_mutex_unlock(&fork_mutex[(this_phil + 1) % n_phil]);
        break;
      default:
        error_out("phil: incorrect philosopher state");
    }
  }
    return NULL;
}

static void emit_symbol(const int symbol) {
  fprintf(output, "%s,", sym_text[symbol]);
}

static int flip_coin(void) {
    return random() % 2;
}

static int flip_Bcoin(int bias) {
  int number = (100.0 * random()/ (RAND_MAX+1.0))+1;
  if ( number <= bias ) return COIN_HEADS;
  return COIN_TAILS;
}

static void error_out(const char *function) {
    perror(function);
    exit(1);
}

void INThandler(int signal) {
  if ( signal == SIGINT ) {
    fflush(output);
    fclose(output);
    exit(EXIT_SUCCESS);
  }
}
