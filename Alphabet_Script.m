function [Alphabet,G] = Alphabet_Script(n)
%n = 5;
labels = dec2bin(2^n-1:-1:0);
stable = {};
k=1;
for i=1:2^n
    num_tokens = 0;
    for j=1:n
        if labels(i,j)==labels(i,mod(j,n)+1)
            num_tokens=num_tokens+1;
        end
    end
    if num_tokens == 1
        stable{k} = strcat('l',labels(i,:));
        k=k+1;
    end
end
G = stable;
Alphabet = {};
for i=1:2^n
    Alphabet{i} = strcat('l',labels(2^n-i+1,:));
end

end