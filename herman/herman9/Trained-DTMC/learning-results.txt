concrete model
----------------------------
SUMMARY:
Experiment model is in: Trained-DTMC/;
Time consuming for prefix tree acceptor construction is: [13.252] seconds;
Time consuming for the overral procedure is: [136004.3784] seconds;
The region of alpha value for the good model is [0.015625,64];
The final model size is [26012];
Its corresponding BIC score is [-67601100.9967] and Log-likelihood is:[-200.5231].
Your MATLAB session has timed out.  All license keys have been returned.

abstract model
----------------------------
SUMMARY:
Experiment model is in: Trained-DTMC/;
Time consuming for prefix tree acceptor construction is: [158.6708] seconds;
Time consuming for the overral procedure is: [275.5361] seconds;
The region of alpha value for the good model is [0.015625,64];
The final model size is [2];
Its corresponding BIC score is [-10304.7875] and Log-likelihood is:[-10291.6558].

abstract-k0
----------------------------
SUMMARY:
Experiment model is in: Trained-DTMC/;
Time consuming for prefix tree acceptor construction is: [114.2125] seconds;
Time consuming for the overral procedure is: [225.4068] seconds;
The region of alpha value for the good model is [0.015625,64];
The final model size is [2];
Its corresponding BIC score is [-10304.7875] and Log-likelihood is:[-10291.6558].

abstract-k5
---------------------------------
SUMMARY:
Experiment model is in: Trained-DTMC/;
Time consuming for prefix tree acceptor construction is: [718.4788] seconds;
Time consuming for the overral procedure is: [1491.7993] seconds;
The region of alpha value for the good model is [0.015625,2048];
The final model size is [4];
Its corresponding BIC score is [-11834.1873] and Log-likelihood is:[-11755.3967].

abstract-k10
---------------------------------
SUMMARY:
Experiment model is in: Trained-DTMC/;
Time consuming for prefix tree acceptor construction is: [112.3087] seconds;
Time consuming for the overral procedure is: [783.1977] seconds;
The region of alpha value for the good model is [0.015625,2048];
The final model size is [3];
Its corresponding BIC score is [-10289.8902] and Log-likelihood is:[-10250.4949].

