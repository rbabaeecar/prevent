dtmc 
 
module model_learned_504724 
s:[1..4] init 1;
[]s=1 -> 33/1000:(s'=2) +  1/1000:(s'=1) +  587/1000:(s'=3) +  379/1000:(s'=4);
[]s=2 -> 495048/495048:(s'=2);
[]s=3 -> 920/7035:(s'=2) +  6011/7035:(s'=3) +  104/7035:(s'=4);
[]s=4 -> 35/642:(s'=2) +  447/642:(s'=3) +  160/642:(s'=4);
endmodule

label "g" = s=2;
label "l111111111" = s=1;
label "V2" = s=3;
label "V3" = s=4;

