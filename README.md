# README #

The RV-2018 reviewers:

To run the abstract symbols extraction run Extract_abst_Alphabet.m
The default is to run the algorithm on the Herman's algorithm with 5 processes. You can change that by changing the dir, and the value of n accordingly.

The following description is to run Prevent to replicate the experiments in [1].

This repository contains the source code that implements Prevent, an online monitoring framework using HMM, and the artifacts of two case studies: the randomized dining philosopher and the flight control of a hexacopter.

The tool is not still fully automated, and needs manual execution. The sequence of execution is as follows:

1- Run Train_HMM.m on the training samples to obtain the HMMs stored in the specified file/folder

2- Run Define_DFA.m to define the DFA that is needed to specify the acceptable extensions

3- Run Create_DTMC.m to obtain the prediction model. The DTMC is the product of the HMM and the DFA, and is saved as a Prism file.

4- Use Prism to open the DTMC file and verify F_accept.props with the 'T' as the desired maximum horizon.

5- Save the log of the Prism and run convert_results_from_prism.py to get the monitor vector from the saved results of Prism (an example is provided at hexacopter/log-prism-MSG_ERROR-full-while06.txt).

6- Run the Monitor_Vector* in Matlab created as the result of the conversion in the previous step to obtain the monitorV

7- Run monitoring.m on one or a set of traces (the HMM, DFA, and monitorV must have been already created)

Version:0.2
Secondary version for the RV-2018 paper

Repo owner: Reza Babaee
Other Admin: Arie Gurfinkel, Sebastian Fischmeister
