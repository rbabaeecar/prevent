function Data=readCSVData(filename)
% Reading the data stored in a CSV file
% From the 'filename'
% Data is a set of sequences, each comprising of the elements in 'Alphabet'
% nObs is the number of observations from the file
Str=importdata(filename);
nObs = length(Str);
Data={};
for i = 1:nObs
    Data{i,1}=split(Str(i),",")';
end
