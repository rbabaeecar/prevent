# This is a simple script to convert the results of Prism to a Matlab file

# The variables MAX_t and MAX_s need to be changed to the ones that are in the Prism results. MAX_t is the maximum T defined in Prism, and MAX_s is the number of states after the model is built in Prism. The file names for monitorFile (output) and the resultsFile (input) are also requried to change accordingly. The defaults are for the hexacopter example.

import re

# TODO read these from the file
MAX_t=1000;
t=1;
MAX_S=81;
with open("Monitor_Vector_Hex_full_while_06.m", "wt") as monitorFile:
    monitorFile.write("monitorV=zeros(" + str(MAX_S+1) + ", 2, " + str(MAX_t) + ");\n\n\n" )
    with  open("hexacopter/log-prism-MSG_ERROR-full-while06.txt", "rt") as resultsFile:
        for line in resultsFile:
            if line[0] == '0':
                monitorFile.write("monitorV(:,:," + str(t) + ")=[\n")
                t = t+1
            if line[0].isdigit():
                # Find the state number
                match=re.findall('(?:\()(\d+)(?:\))',line)
                if match:
                    monitorFile.write(match[0]+" ")
                # Find the probability
                match=re.findall('(?:\=)(.*)',line)
                if match:
                    monitorFile.write(match[0] + ";\n")
            if line.startswith(str(MAX_S)):
                monitorFile.write("];\n\n")

resultsFile.close()
monitorFile.close()
