
% Run the specific segment of an experiment to create the DFA

% Randomized dining philosopher
% The DFA defined for G( hungry => F eat )
Alphabet={'think','hungry','try','pick','drop','eat'};
Q=[{'q0'} {'q1'}];
delta=[{'q0'} {'q1'} {'q0'} {'q0'} {'q0'} {'q0'}; {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q0'}];
InitialStates={'q0'};
FinalStates=[{'q0'}];
RejectStates={'q1'};
dfa=DFA(Q,Alphabet,delta,InitialStates,FinalStates,RejectStates,[],[]);

% Hexacopter
% The DFA defined for G(Not MSG_ERROR)
Alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'h', 'i', 'j', 'k', 'l', 'n', 'o', 'p', 'q', 'r', 't'};
Q=[{'q0'} {'q1'}];
delta=[{'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q1'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'} {'q0'}; {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'} {'q1'}];
InitialStates={'q0'};
FinalStates=[{'q1'}];
RejectStates={'q0'};
dfa=DFA(Q,Alphabet,delta,InitialStates,FinalStates,RejectStates,[],[]);