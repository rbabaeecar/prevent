% Monitoring

% we assume the monitoring vector,  'monitorV' is already populated with the
% results from Prism.

% Reading monitoring trace -- can be changed to any other trace
dir = "randomized-dining-philosophers/LR-stay-at-pick/Traces/Monitoring/";
execFile = "LR-sap_3Phils_B0.csv";

% The set of execution traces that the monitoring is performed on
execTrace = readCSVData(dir+execFile);
nObs = length(execTrace);

% Maximum horizon for the prediction -- currently obtained from the monitor
% vector (the maximum possible value of MAX_T)
MAX_T = size(monitorV, 3);
% The probability prediction computed by the monitor for each step of the
% trace
probVector={};
% The monitor estimated length of the extension that is accepted by the DFA
estLambda={};
% The actual length of the extension that is accepted by the DFA
lambda={};
% The for iterates through each trace in the moniotring set, if there is
% one (usual case), this loop only executes one
for j=1:nObs
    % Get a sample execution and simulate running it
    Exec=execTrace{j};
    % The first state of the DFA
    q=dfa.InitialState;
    % Initializing vectors
    probVector{j}=[];
    estLambda{j}=[];
    lambda{j}=[];
    % The previous index that the prefix was accepted by the DFA. It's used
    % to determine the length of the new accepted extension
    prevAccIdx = 0;
    % the horizon index
    T=MAX_T;
    % Viterbi vectors
    Vit = estEmis(:,find(strcmp(Exec(1),Alphabet)))'.*estTrans(1,:);
    VitNext = Vit;
    % The monitoring for each symbol in the execution trace
    for i=1:length(Exec)
        % finding the most probable state using Viterbi vector
        [value,s]=max(Vit);
        % finding the state of the DFA
        q=GetTransitionState(dfa,q,Exec(i));
        % The emitted symbol
        o=find(strcmp(Exec(i),Alphabet));
        % CState is the index of the composite state at the DTMC (the prediction model)
        CState=(s-1)*length(Alphabet)*length(dfa.FiniteSetOfStates)+(o-1)*length(dfa.FiniteSetOfStates)+find(strcmp(q,dfa.FiniteSetOfStates));
        % Finding the state in the monitor vector
        index=find(monitorV(:,1,T)==CState);
        % If there is a proabability defined for the composite state
        if ~isempty(index)
            probVector{j}(i)=monitorV(index,2,T);
        end
        % if the prefix is accepted by the DFA
        if (strcmp(q,dfa.FinalAcceptStates))
            % computing the value of lambda
            for k=prevAccIdx+1:i-1
                lambda{j}(k) = i-k;
            end
            % computing the estimation of lambda
            for k=prevAccIdx+1:i-1
                if(k-prevAccIdx < MAX_T)
                    estLambda{j}(k) = probVector{j}(k)*(i-k);
                else
                    estLambda{j}(k) = 0;
                end
            end
            prevAccIdx = i;
            % Reseting the horizon index
            T = MAX_T;
        % if the prefix is not accepted by the DFA
        else
            % Decrease the horizon
            T = T-1;
        end
        % There is no accepting extension of length MAX_T
        if( T <= 0 )        
            % Reseting the horizon index
            T = MAX_T;
        end
        % Updating the Viterbi Vector for the next iteration
        if i < length(Exec)
            for k=1:size(estEmis,1)
                VitNext(k)=estEmis(k,find(strcmp(Exec(i+1),Alphabet)))*max(Vit.*estTrans(:,k)');
            end
            Vit = VitNext;
        end
    end
end

% Calculating the mean of estLambda for each trace
estMeanVector=[];
for i=1:nObs
    estMeanVector(i)=mean(estLambda{i});
end
