function [F] = ComputePredictionSupport(Data, sigma, G, k)
% Computes the k-prediction support of sigma w.r.t. G over Data

% computing the sum of F
    F = zeros(length(Data),1);
    % using the parfor from tha parallel toolbox to speed up
    % computation of F
    parfor n=1:length(Data)
        % if k is larger than the length of the data then skip
        if k >= length(Data{n})-1
            continue;
        end
        % Vectorization
        % Finding the position of symbols in R over Data{n}
        Rpos=ismember(Data{n},sigma);
        % Finding the position of symbols in G (target labels) over Data{n}
        Gpos=ismember(Data{n},G);
        % Shifting Gpos k steps to the right
        shiftedGpos=zeros(size(Gpos));
        shiftedGpos(1:end-(k+1))=Gpos(k+2:end);
        % Computing the number of times that Data{n}(i)=sigma in R and
        % Data{n}(i+k)=sigma' in G
        prod=Rpos.*shiftedGpos;
        % Computing F as the sum
        F(n,1)=sum(prod(1:length(Data{n})-k-1));
        % Computing the prediction support
        F(n,1) = F(n,1) / (length(Data{n}) - k - 1);
    end
end

