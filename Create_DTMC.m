% The DFA and the HMM must have been built already

filename='Prediction-Models/MSG_ERR-full-while-06.prism';
% filename= 'phil3-LR';

% number of hidden states. estEmis contains the estimated
% emisstion probabilities
s = size(estEmis,1);
% number of observations
o = size(estEmis,2);
% number of states in DFA
d = length(dfa.FiniteSetOfStates);

fid = fopen(filename,'w+');
fprintf(fid,'dtmc \n \n');
% creating a label for the accepting states
fprintf(fid,'label \"accept\" = (');

% Creating the DTMC 
Tr_mc_prod=zeros(s*o*d);
% the indices of Tr_mc_prod
p=1;
firstState=true;
q=1;
for i=1:s
    for l=1:o
        for j=1:d
            for k=1:s
               for m=1:o
                   for n=1:d
                        if strcmp(dfa.FiniteSetOfStates(n),GetTransitionState(dfa,dfa.FiniteSetOfStates(j),Alphabet(m))) && estEmis(k,m)~=0
                            Tr_mc_prod(p,q)=estTrans(i,k)*estEmis(k,m);
                        end
                        q=q+1;
                    end
               end
            end
            if strcmp(dfa.FinalAcceptStates, dfa.FiniteSetOfStates(j))
                if(firstState)
                    fprintf(fid,'(s=%d)',p);
                    firstState=false;
                else
                    fprintf(fid,'| (s=%d)',p);
                end
            end
            p=p+1;
            q=1;
        end
    end
end
fprintf(fid,');\n');

% Normalizing the values of Tr_mc_prod
for i=1:s*o*d
    if sum(Tr_mc_prod(i,:))~=0
        Tr_mc_prod(i,:)=Tr_mc_prod(i,:)./sum(Tr_mc_prod(i,:));
    end
end

% Creating the DTMC module in the Prism file
fprintf(fid,'module %s \n',"monitor");
fprintf(fid,'s:[1..%d] init %d;\n',s*o*d, 1);
for i=1:s*o*d
    if all(Tr_mc_prod(i,:)==0)
        continue;
    end
    fprintf(fid,'[]s=%d -> ',i);
    prob=0;
    for j=1:s*o*d
        if Tr_mc_prod(i,j)>1e-10
            prob=prob+Tr_mc_prod(i,j);
            fprintf(fid, '%.6f:(s''=%d)',Tr_mc_prod(i,j),j);
            % it is the last state in this line
            if prob>=9999999999e-10
                fprintf(fid, ';');
            % there are still some states on this line
            else
                fprintf(fid, ' + ');
            end
        end
    end
    fprintf(fid,'\n');
end
fprintf(fid,'endmodule\n\n');
fclose(fid);