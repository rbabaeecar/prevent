% Read the data
dir = 'herman/herman5/';
filename='normal_traces.csv';
Data =readCSVData(strcat(dir,filename));

% The number of processes in the herman example
n = 5; 
% The actual alphabet and the set of target symbols in G
% set of labels that show the stable configuration in the harman algorithm
[Alphabet,G] = Alphabet_Script(n);

% h as the prediction horizon
% is used to stop finding the similar symbols up that are away up to h
% steps. If h>the length of the largest path in the actual model, that covers all
% the paths. For herman, h>=2^n would cover all the paths in the model
h=32;
% The value of k in the k-gap pair model
% k=0 demonstrates the bigram model
k = 10;

% V the set of equivalence classes
V = {};
vidx = 1;
% The set of remaining symbols that are not still classified
R = setdiff(Alphabet,G);

tic;
while (h > 0 && ~isempty(R))
    % finding the sigma_max
    F_sigma_max=0;
    sigma_max_idx=0;
    for i=1:length(R)
        if mod(i,length(R)/5)==1
            fprintf('.')
        end
        % The following line is replaced by the block after to compute the sum of F on-the-fly
        % F_sigma=sum(F(:,find(strcmp(Alphabet,R(i))),k));
        F=ComputePredictionSupport(Data,R(i),G,k);
        F_sigma=sum(F(:,1));
        % comparing it to the F_sigma_max
        if F_sigma > F_sigma_max
            sigma_max_idx=i;
            F_sigma_max=F_sigma;
            F_max=F;
        end
    end
    % if there is no path of length k, then break
    if sigma_max_idx == 0
        break;
    end
    % Find other symbols with similar prediction support to the sigma_max
    sigma_max = R(sigma_max_idx);
    R = setdiff(R,sigma_max);
    V{vidx}=sigma_max;
    for i=1:length(R)
        F=ComputePredictionSupport(Data,R(i),G,k);
        % if the test is accepted, i.e. ttest == 0
            if ~ttest(F_max(:,1)-F(:,1))
                V{vidx} = union(V{vidx},R(i));
            end
    end
    R = setdiff(R,V{vidx});
    h = h-1;
    vidx = vidx+1;
end
fprintf('\n time: %f\n', toc);


% Generating the bash script to convert the traces into abstract traces,
% this is essentially the relation R in the paper
% and storing the abstract alphabet in abstAlphabet
abstAlphabet = {};
fprintf('Abstract Alphabet:\n');
scriptFileName = strcat('convert2abstract_traces_k', num2str(k), '.bash');
fid = fopen(strcat(dir,scriptFileName),'w+');
fprintf(fid,"#!/bin/bash\n");
abstractTraceFileName = strcat('abstract_traces_k', num2str(k),'.csv');
fprintf(fid,strcat('file=\"',abstractTraceFileName,'\"\n'));
abstAlphabet{1} = 'g';
fprintf('%s\t', abstAlphabet{1});
for i=1:length(G)
    fprintf(fid,"sed -i \'s/" + string(G(i)) + "/g/g\' $file\n");
end
for i=1:length(V)
    if length(V{i}) > 1
        abstAlphabet{i+1} = strcat('V', num2str(i));
        fprintf('%s\t', abstAlphabet{i+1});
        for j=1:length(V{i})
            fprintf(fid,"sed -i \'s/" + string(V{i}(j)) + "/V" + num2str(i) + "/g\' $file\n");
        end
    else
        abstAlphabet{i+1} = char(V{i});
        fprintf('%s\t', abstAlphabet{i+1});
    end
end
if ~isempty(R)
    for i=1:length(R)
        fprintf(fid,"sed -i \'s/" + string(R(i)) + "/N/g\' $file\n");
    end
    abstAlphabet{length(V)+2} = 'N';
    fprintf('%s\n', abstAlphabet{length(V)+2});
end
fprintf('\n');
save(dir+"Abstract_Alphabet_k"+num2str(k)+".mat",'abstAlphabet');
fclose(fid);